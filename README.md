# AppSys PdfParser PlugIn #

This is a free plugin that enables users to use PdfParser  process PDF files inside the AppSys framework.

# PdfParser #

Pdf Parser, a standalone PHP library, provides various tools to extract data from a PDF file.

Website : [http://www.pdfparser.org](http://www.pdfparser.org/?utm_source=GitHub&utm_medium=website&utm_campaign=GitHub)

Test the API on our [demo page](http://www.pdfparser.org/demo).

This project is supported by [Actualys](http://www.actualys.com).

## Features ##

Features included :

- Load/parse objects and headers
- Extract meta data (author, description, ...)
- Extract text from ordered pages
- Support of compressed pdf
- Support of MAC OS Roman charset encoding
- Handling of hexa and octal encoding in text sections
- PSR-0 compliant ([autoloader](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-0.md))
- PSR-1 compliant ([code styling](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-1-basic-coding-standard.md))

Currently, secured documents are not supported.

This Library is still under active development.
As a result, users must expect BC breaks when using the master version.

## Documentation ##

[Read the documentation on website](http://www.pdfparser.org/documentation?utm_source=GitHub&utm_medium=documentation&utm_campaign=GitHub).

Original PDF References files can be downloaded from this url : http://www.adobe.com/devnet/pdf/pdf_reference_archive.html


## License ##

This library is under the [LGPLv3 license](https://github.com/smalot/pdfparser/blob/master/LICENSE.txt).

