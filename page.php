<?php

class PlugIn {
	
	public $js = 'pdfparser.js';
	
	public function page() {
		$html = "
		<h4>PDF Parser</h4>
		<form action='plugback.php' method='post' enctype='multipart/form-data' onsubmit='pdfparser.sendFile(event,this);'>
			<div class='row'>
				<div class='col col-100'>
					Select PDF:
					<input type='file' name='pdfFile[]' id='pdfFile'>
					<input type='submit' name='submit' value='Upload PDF'>
				</div>
			</div>
		</form>
		<span class='space'></span>
		<hr>
		<div id='pdfdata'></div>
		";
		
		return $html;
	}
	
}
