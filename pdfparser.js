
var pdfparser = new function PDFparser() {

	var parserobj = this;

	this.sendFile = function(event,form) {
		event.preventDefault();
		
		var pdf = form.elements[0].files[0];
		
		var formData = new FormData();
		formData.append('pdfUpload', pdf, pdf.name);
		
		formData.append('plugin','appsys-pdfparser');
        formData.append('path','pdfback.php');
		
		var url = location.href.substring(0, location.href.lastIndexOf("/")+1) + "plugback.php";
		
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.open("POST", url, true);
		xmlhttp.onreadystatechange = function() {
	        if (xmlhttp.readyState == XMLHttpRequest.DONE ) {
				if(xmlhttp.status == 200) {
		        	parserobj.pdfData(JSON.parse(xmlhttp.responseText));
				} else {
					alert("Error:" + xmlhttp.status + ": Please Try Again Later");
				}
	        }
	    }
		    
		xmlhttp.send(formData);
	}
	
	this.pdfData = function(data) {
		var div = document.getElementById('pdfdata');
		div.innerHTML = "";
		
		for(var key in data) {
			if (!data.hasOwnProperty(key)) continue;
			
			div.innerHTML += `<pre style="white-space: pre-wrap;">` + data[key] + `</pre><div class="space"></div>`;
		}
		
	}

}
