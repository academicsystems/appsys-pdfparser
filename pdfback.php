
$api = new AppSysAPI();

if(!$api->include('vendor/autoload.php'))
{
	echo '{"error":"failed to require pdfparser"}'; die();
}
	
$count = count($_FILES);
$keys = array_keys($_FILES);

if($count <= 0)
{
	echo '{"error":"no file"}'; die();
}

$files = array();
for($i = 0; $i < $count; $i++)
{
	$current = $keys[$i];
	
	/* no error & less than 10 mb */
	if($_FILES[$current]['error'])
	{
		$result[$_FILES[$current]['name']] = 'File Error.';
		continue;
	}

	$path = '/tmp/' . $_FILES[$current]['name'];
	if(file_exists($path))
	{
		unlink($path);
	}
	
	if(move_uploaded_file($_FILES[$current]['tmp_name'], $path))
	{
		$files[$path] = 'success';
	}
	else
	{
		$files[$path] = 'error uploading';
	}
}

$parser = new \Smalot\PdfParser\Parser();

$filedata = array();
foreach($files as $fpath => $value)
{
	if($value === "success")
	{
		$pdf = $parser->parseFile($fpath);
		$filedata[explode('/tmp/', $fpath)[1]] = nl2br($pdf->getText());
	}
	else
	{
		$filedata[explode('/tmp/', $fpath)[1]] = $value;
	}	
}

echo json_encode($filedata);


